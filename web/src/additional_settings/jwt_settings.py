from datetime import timedelta
from os import environ

JWT_AUTH_REFRESH_COOKIE = environ.get('JWT_AUTH_REFRESH_COOKIE', 'refresh')
JWT_AUTH_COOKIE = environ.get('JWT_AUTH_COOKIE', 'jwt-auth')


SIMPLE_JWT = {
    'ACCESS_TOKEN_LIFETIME': timedelta(days=30),
    'REFRESH_TOKEN_LIFETIME': timedelta(days=30 * 6),
    'ROTATE_REFRESH_TOKENS': True,
    'BLACKLIST_AFTER_ROTATION': True,

    'ALGORITHM': environ.get('ALGORITHM', 'HS256'),
    'SIGNING_KEY': environ.get('SIGNING_KEY') or environ.get('SECRET_KEY'),
    'VERIFYING_KEY': environ.get('VERIFYING_KEY'),
}
