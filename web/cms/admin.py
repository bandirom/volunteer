from django.contrib import admin

from . import models


@admin.register(models.Volunteer)
class VolunteerAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name', 'phone_number', 'city', 'added_by')
    exclude = ('added_by',)
    list_filter = ('added_by',)
    search_fields = ('phone_number',)

    def save_model(self, request, obj: models.Volunteer, form, change: bool):
        if not obj.added_by:
            obj.added_by = request.user
        obj.save()


@admin.register(models.City)
class CityAdmin(admin.ModelAdmin):
    list_display = ('name', 'region')


@admin.register(models.Event)
class EventAdmin(admin.ModelAdmin):
    list_display = ('name',)


@admin.register(models.OrganizationEvent)
class OrganizationEventAdmin(admin.ModelAdmin):
    list_display = ('name', 'date')
    filter_horizontal = ('participants',)
    list_select_related = ('name', 'organization')
    exclude = ('organization',)
    date_hierarchy = 'date'

    def save_model(self, request, obj: models.OrganizationEvent, form, change: bool):
        if not change:
            obj.organization = request.user.organization
        obj.save()
