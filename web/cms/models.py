from django.contrib.auth import get_user_model
from django.db import models
from django.utils.translation import gettext_lazy as _

from main.models import City, Organization

User = get_user_model()


class Volunteer(models.Model):
    first_name = models.CharField(_('First name'), max_length=100)
    last_name = models.CharField(_('Last name'), max_length=100)
    patronymic = models.CharField(_('Patronymic'), max_length=100)
    phone_number = models.CharField(_('Phone number'), max_length=20)
    city = models.ForeignKey(
        City,
        on_delete=models.CASCADE,
        related_name='volunteers',
        verbose_name=_('City'),
        null=True,
        blank=True,
    )
    added_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True, verbose_name=_('Added by'))
    created = models.DateTimeField(_('Created'), auto_now_add=True)
    organization = models.ForeignKey(
        Organization,
        on_delete=models.SET_NULL,
        null=True,
        related_name='volunteers',
        verbose_name=_('Organization'),
    )
    comment = models.TextField(default='', blank=True)

    def __str__(self) -> str:
        return f'{self.full_name} - {self.city}'

    @property
    def full_name(self) -> str:
        return f'{self.first_name} {self.last_name}'.strip()

    class Meta:
        verbose_name = _('Volunteer')
        verbose_name_plural = _('Volunteers')


class Event(models.Model):
    name = models.CharField(_('Event'), max_length=100)

    def __str__(self) -> str:
        return self.name

    class Meta:
        verbose_name = _('Event')
        verbose_name_plural = _('Events')


class OrganizationEvent(models.Model):
    name = models.ForeignKey(
        Event,
        on_delete=models.CASCADE,
        related_name='all_events',
        verbose_name=_('Name'),
        unique_for_date='date',
    )
    date = models.DateTimeField(_('Date'))

    organization = models.ForeignKey(
        Organization,
        on_delete=models.CASCADE,
        related_name='events',
        verbose_name=_('Organization Event'),
    )
    participants = models.ManyToManyField(Volunteer, verbose_name=_('Participants'))

    class Meta:
        verbose_name = _('Organization Event')
        verbose_name_plural = _('Organization Events')
