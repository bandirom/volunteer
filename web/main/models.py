from typing import TypeVar

from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import gettext_lazy as _

from .choices import Region
from .managers import UserManager

UserType = TypeVar('UserType', bound='User')


class Speciality(models.Model):
    name = models.CharField(_('Name'), max_length=100, unique=True)
    organization = models.ForeignKey(
        'Organization',
        on_delete=models.CASCADE,
        related_name='specialities',
    )

    class Meta:
        verbose_name = _('Speciality')
        verbose_name_plural = _('Specialities')

    def __str__(self) -> str:
        return f'{self.name}'


class Organization(models.Model):
    name = models.CharField(max_length=120)
    phone_number = models.CharField(max_length=20)
    address = models.OneToOneField('Address', on_delete=models.CASCADE, related_name='organization', null=True)

    class Meta:
        verbose_name = _('Organization')
        verbose_name_plural = _('Organizations')

    def __str__(self) -> str:
        return f'{self.name}'


class User(AbstractUser):

    username = None
    email = models.EmailField(_('Email address'), unique=True)
    organization = models.ForeignKey(
        Organization,
        on_delete=models.CASCADE,
        related_name='users',
        null=True,
        blank=True,
        verbose_name=_('Organization'),
    )
    speciality = models.ForeignKey(
        Speciality,
        on_delete=models.SET_NULL,
        null=True,
        related_name='users',
        verbose_name=_('Speciality'),
    )
    telegram_number = models.CharField(_('Telegram number'), max_length=25, null=True, blank=True)
    telegram_chat_id = models.CharField(_('Telegram chat id'), max_length=150, null=True, blank=True)

    USERNAME_FIELD: str = 'email'
    REQUIRED_FIELDS: list[str] = []

    objects = UserManager()

    class Meta:
        verbose_name = _('User')
        verbose_name_plural = _('Users')

    def __str__(self) -> str:
        return self.email

    def full_name(self) -> str:
        return super().get_full_name()


class City(models.Model):
    name = models.CharField(_('Name'), max_length=100)
    region = models.CharField(_('Region'), choices=Region.choices, max_length=100)

    def __str__(self) -> str:
        return f'{self.name} - {self.region}'

    class Meta:
        verbose_name = _('City')
        verbose_name_plural = _('Cities')


class Address(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='addresses', null=True, blank=True)
    address = models.CharField(_('Address'), default='', max_length=255)
    city = models.ForeignKey(City, on_delete=models.CASCADE, related_name='addresses')
    zip_code = models.CharField(_('Zip code'), default='', max_length=255, blank=True)

    def __str__(self) -> str:
        return f'{self.address}, {self.city}, {self.zip_code}'

    class Meta:
        verbose_name = _('Address')
        verbose_name_plural = _('Addresses')
