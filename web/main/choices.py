from django.db.models import TextChoices
from django.utils.translation import gettext_lazy as _


class Region(TextChoices):
    KYIV = ('kyiv', _('Kyiv region'))
    LVIV = ('lviv', _('Lviv region'))
    CHERNIHIV = ('chernihiv', _('Chernihiv region'))
    KHARKIV = ('kharkiv', _('Kharkiv region'))
    ODESSA = ('odessa', _('Odessa region'))
    CHERKASY = ('cherkasy', _('Cherkasy region'))


