# Generated by Django 3.2.11 on 2022-04-16 18:03

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0003_auto_20220416_1835'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Addresses',
            new_name='Address',
        ),
    ]
