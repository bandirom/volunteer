from django.conf import settings
from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import Group
from django.utils.translation import gettext_lazy as _

from main.models import Address, Organization, Speciality

User = get_user_model()


class AddressInline(admin.StackedInline):
    model = Address
    extra = 0


@admin.register(Speciality)
class SpecialityAdmin(admin.ModelAdmin):
    list_display = ('name', 'organization')
    list_select_related = ('organization',)


@admin.register(Organization)
class OrganizationAdmin(admin.ModelAdmin):
    list_display = ('name', 'phone_number', 'address')


@admin.register(Address)
class AddressAdmin(admin.ModelAdmin):
    list_display = ('address', 'city', 'zip_code')


@admin.register(User)
class CustomUserAdmin(UserAdmin):
    ordering = ('-id',)
    list_display = ('email', 'full_name', 'is_active', 'organization')
    search_fields = ('first_name', 'last_name', 'email', 'telegram_number')

    fieldsets = (
        (_('Personal info'), {
            'fields': ('id', 'first_name', 'last_name', 'email', 'organization', 'telegram_number', 'speciality')
        }),
        (_('Secrets'), {'fields': ('password', 'telegram_chat_id')}),
        (
            _('Permissions'),
            {
                'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions'),
            },
        ),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (
            None,
            {
                'classes': ('wide',),
                'fields': ('email', 'organization', 'password1', 'password2'),
            },
        ),
    )
    readonly_fields = ('id',)


title = settings.MICROSERVICE_TITLE

admin.site.site_title = title
admin.site.site_header = title
admin.site.site_url = '/'
admin.site.index_title = title

admin.site.unregister(Group)
