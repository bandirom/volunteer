from rest_framework.permissions import BasePermission

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from rest_framework.request import Request
    from rest_framework.views import APIView


class OrganizationPermission(BasePermission):

    def has_permission(self, request: 'Request', view: 'APIView') -> bool:
        return bool(request.user.is_authenticated and request.user.organization)
