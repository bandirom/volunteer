from typing import TYPE_CHECKING

from django.contrib.auth import get_user_model

from main.decorators import except_shell

if TYPE_CHECKING:
    from main.models import UserType


User = get_user_model()


class AuthService:

    @staticmethod
    @except_shell((User.DoesNotExist,))
    def get_user(email: str) -> 'UserType':
        return User.objects.get(email=email)
