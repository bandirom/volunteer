from collections import OrderedDict
from itertools import groupby

from django.db.models import QuerySet

from cms.models import City, Volunteer, Event


class CmsService:

    @staticmethod
    def volunteer_queryset(organization) -> QuerySet[Volunteer]:
        return Volunteer.objects.filter(organization=organization)

    @staticmethod
    def is_volunteer_exists(first_name, last_name, phone_number) -> bool:
        return Volunteer.objects.filter(
            first_name__iexact=first_name, last_name__iexact=last_name, phone_number__icontains=phone_number
        ).exists()

    @staticmethod
    def get_cities() -> QuerySet[City]:
        return City.objects.all()

    @staticmethod
    def ordered_cities() -> dict:
        regions = OrderedDict()
        cities = CmsService.get_cities()
        for region, group in groupby(cities, lambda city: city.region):
            regions[region] = []
            for city_obj in group:
                regions[region].append(city_obj.name)
        return regions

    @staticmethod
    def event_queryset():
        return Event.objects.all()
