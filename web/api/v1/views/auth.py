from dj_rest_auth.utils import jwt_encode
from rest_framework.generics import GenericAPIView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from api.v1.serializers import auth as serializers


class LoginView(GenericAPIView):
    serializer_class = serializers.LoginSerializer
    response_serializer_class = serializers.JWTSerializer

    permission_classes = (AllowAny,)

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']

        access_token, refresh_token = jwt_encode(user)
        data = {
            'access_token': access_token,
            'refresh_token': refresh_token,
            'user': user,
        }
        serializer = self.response_serializer_class(data)
        return Response(serializer.data)
