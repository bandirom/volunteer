from rest_framework.generics import ListCreateAPIView, ListAPIView
from rest_framework.response import Response
from rest_framework.views import APIView

from api.v1.serializers import cms as serializers
from api.v1.services.cms import CmsService


class VolunteerView(ListCreateAPIView):
    serializer_class = serializers.VolunteerSerializer

    def get_queryset(self):
        return CmsService.volunteer_queryset(self.request.user.organization)


class RegionCitiesView(APIView):

    def get(self, request):
        data = CmsService.ordered_cities()
        return Response(data)


class EventListView(ListAPIView):
    serializer_class = serializers.EventSerializer

    def get_queryset(self):
        return CmsService.event_queryset()
