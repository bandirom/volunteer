from drf_yasg.utils import swagger_auto_schema
from rest_framework.generics import CreateAPIView

from api.v1.serializers import orders as serializers
from api.v1.swagger_schemas import orders as schemas


class CreateOrderView(CreateAPIView):
    """Payload:
    volunteer object or volunteer_id must present
    """

    serializer_class = serializers.CreateOrderSerializer

    @swagger_auto_schema(**schemas.create_order_schema)
    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)
