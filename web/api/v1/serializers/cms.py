import requests
from django.conf import settings
from django.contrib.auth import get_user_model
from django.utils.translation import gettext_lazy as _
from rest_framework import serializers

from api.v1.services.cms import CmsService
from cms.models import Volunteer, Event
from main.models import Speciality

User = get_user_model()


class VolunteerSerializer(serializers.ModelSerializer):
    comment = serializers.CharField(required=False)
    department = serializers.PrimaryKeyRelatedField(queryset=Speciality.objects.all(), required=False)

    def send_message(self, chat_ids: list[str], message: str):
        token = settings.TELEGRAM_BOT_TOKEN
        for chat_id in chat_ids:
            send_text = f'https://api.telegram.org/bot{token}/sendMessage?chat_id={chat_id}&parse_mode=Markdown&text={message}'
            response = requests.get(send_text)
        return

    def validate(self, data: dict) -> dict:
        if CmsService.is_volunteer_exists(data['first_name'], data['last_name'], data['phone_number']):
            raise serializers.ValidationError({'first_name': _('Volunteer already exists')})
        return data

    def create(self, validated_data: dict) -> Volunteer:
        validated_data['added_by'] = self.context['request'].user
        department: Speciality = validated_data.pop('department', None)

        volunteer: Volunteer = super().create(validated_data)
        if department:
            chat_ids = list(User.objects.filter(speciality=department).values_list('telegram_chat_id', flat=True))

            message = f"""Волонтер {volunteer.full_clean} виявив бажання прийняти участь "{department.name}"
            Коментар: {volunteer.comment}
            """
            self.send_message(chat_ids, message)
        return volunteer

    class Meta:
        model = Volunteer
        fields = ('id', 'first_name', 'last_name', 'patronymic', 'phone_number', 'city', 'comment', 'department')


class EventSerializer(serializers.ModelSerializer):
    class Meta:
        model = Event
        fields = ('id', 'name')
