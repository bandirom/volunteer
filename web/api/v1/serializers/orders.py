from typing import TYPE_CHECKING

from django.db import transaction
from django.utils.translation import gettext_lazy as _
from rest_framework import serializers

from api.v1.serializers.cms import VolunteerSerializer
from order.models import Order

if TYPE_CHECKING:
    from main.models import UserType


class CreateOrderSerializer(serializers.ModelSerializer):
    volunteer_id = serializers.IntegerField(min_value=0, required=False)
    volunteer = VolunteerSerializer(required=False)

    def validate(self, data: dict) -> dict:
        if not data.get('volunteer_id') and not data.get('volunteer'):
            raise serializers.ValidationError({'volunteer': _('Volunteer data must be present')})
        return data

    @transaction.atomic()
    def create(self, validated_data: dict):
        user: 'UserType' = self.context['request'].user
        organization = user.organization
        validated_data['organization'] = organization
        validated_data['user'] = user
        if volunteer_id := validated_data.pop('volunteer_id', None):
            validated_data['customer_id'] = volunteer_id
            validated_data['consumer_id'] = volunteer_id
        else:
            volunteer_data: dict = validated_data.pop('volunteer')
            volunteer_data['city'] = volunteer_data['city'].id
            serializer = VolunteerSerializer(data=volunteer_data, context=self.context)
            serializer.is_valid(raise_exception=True)
            volunteer_obj = serializer.save()
            validated_data['customer'] = volunteer_obj
            validated_data['consumer'] = volunteer_obj
        return super().create(validated_data)

    class Meta:
        model = Order
        fields = ('id', 'type', 'comment', 'volunteer', 'volunteer_id')
