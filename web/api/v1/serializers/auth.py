from django.contrib.auth import authenticate, get_user_model
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from rest_framework import serializers

from api.v1.services.auth import AuthService
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from main.models import UserType

User = get_user_model()


error_messages = {
    'not_verified': _('Email not verified'),
    'not_active': _('Your account is not active. Please contact Your administrator'),
    'wrong_credentials': _('Entered email or password is incorrect'),
    'already_registered': _('User is already registered with this e-mail address'),
    'password_not_match': _('The two password fields did not match'),
}


class UserSerializer(serializers.ModelSerializer):
    permissions = serializers.SerializerMethodField()

    def get_permissions(self, obj: 'UserType') -> dict:
        return {
            'organization': obj.organization.name,
            'speciality': getattr(obj.speciality, 'name', _('Not available')),
            'is_admin': obj.is_superuser,
        }

    class Meta:
        model = User
        fields = ('id', 'full_name', 'permissions')


class JWTSerializer(serializers.Serializer):
    access_token = serializers.CharField()
    refresh_token = serializers.CharField()
    user = UserSerializer(read_only=True)


class LoginSerializer(serializers.Serializer):
    email = serializers.EmailField()
    password = serializers.CharField(min_length=8, write_only=True)
    chat_id = serializers.CharField(required=False)

    def authenticate(self, **kwargs):
        return authenticate(self.context['request'], **kwargs)

    def _validate_email(self, email: str, password: str):
        user = self.authenticate(email=email, password=password)
        if not user:
            raise serializers.ValidationError({'email': error_messages['wrong_credentials']})
        return user

    def validate(self, attrs: dict) -> dict:
        email: str = attrs.get('email')
        password: str = attrs.get('password')
        user = self._validate_email(email, password)
        if user:
            if not user.is_active:
                msg = {'email': error_messages['not_active']}
                raise serializers.ValidationError(msg)
        else:
            user = AuthService.get_user(email)
            if not user:
                raise serializers.ValidationError({'email': error_messages['wrong_credentials']})
            if not user.is_active:
                raise serializers.ValidationError({'email': error_messages['not_active']})
            raise serializers.ValidationError({'email': error_messages['wrong_credentials']})
        user.last_login = timezone.now()
        if not user.telegram_chat_id and attrs.get('chat_id'):
            user.telegram_chat_id = attrs.get('chat_id')
        user.save(update_fields=['last_login', 'telegram_chat_id'])
        attrs['user'] = user
        return attrs
