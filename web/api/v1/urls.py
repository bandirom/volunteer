from django.urls import path

from .views import auth, cms, orders

urlpatterns = [
    path('volunteer/', cms.VolunteerView.as_view(), name='volunteer-list'),
    path('regions/', cms.RegionCitiesView.as_view(), name='regions-list'),
    path('login/', auth.LoginView.as_view(), name='login'),
    path('order/', orders.CreateOrderView.as_view(), name='order-list'),
    path('events/', cms.EventListView.as_view(), name='event-list'),
]
