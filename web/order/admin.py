from django.contrib import admin

from order import models


class OrderItemInline(admin.StackedInline):
    model = models.OrderItem
    extra = 1


@admin.register(models.Item)
class ItemAdmin(admin.ModelAdmin):
    list_display = ('name',)


@admin.register(models.Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ('customer', 'id', 'user', 'type', 'status', 'created')
    exclude = ('items', 'user', 'organization')
    inlines = (OrderItemInline,)
    list_filter = ('type', 'customer')
    date_hierarchy = 'updated'

    def save_model(self, request, obj: models.Order, form, change: bool):
        if not change:
            obj.user = request.user
            obj.organization = request.user.organization
        obj.save()


@admin.register(models.OrderItem)
class ItemOrderAdmin(admin.ModelAdmin):
    list_display = ('item', 'order', 'count')
