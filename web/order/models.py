from django.contrib.auth import get_user_model
from django.db import models
from django.utils.translation import gettext_lazy as _

from cms.models import Volunteer
from main.models import Organization
from order.choices import OrderStatus, OrderType

User = get_user_model()


class Item(models.Model):
    name = models.CharField(_('Name'), max_length=120)

    class Meta:
        verbose_name = _('Item')
        verbose_name_plural = _('Items')

    def __str__(self) -> str:
        return self.name


class Order(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='orders', verbose_name=_('User'))
    status = models.IntegerField(_('Status'), choices=OrderStatus.choices, default=OrderStatus.CREATED)
    type = models.CharField(_('Type'), choices=OrderType.choices, max_length=50, null=True, blank=True)
    items = models.ManyToManyField(Item, related_name='orders', verbose_name=_('Items'))
    organization = models.ForeignKey(Organization, on_delete=models.SET_NULL, null=True, related_name='orders')
    customer = models.ForeignKey(
        Volunteer, on_delete=models.SET_NULL, null=True, related_name='customer_orders', verbose_name=_('Customer')
    )
    consumer = models.ForeignKey(
        Volunteer, on_delete=models.SET_NULL, null=True, related_name='consumer_orders', verbose_name=_('Consumer')
    )
    created = models.DateTimeField(_('Created'), auto_now_add=True)
    updated = models.DateTimeField(_('Updated'), auto_now=True)
    comment = models.TextField(default='', blank=True)

    def __str__(self) -> str:
        return f'{_("Order")}: #{self.pk}, {_("Status")}: {self.get_status_display()}'

    class Meta:
        verbose_name = _('Order')
        verbose_name_plural = _('Orders')


class OrderItem(models.Model):
    item = models.ForeignKey(Item, on_delete=models.CASCADE, verbose_name=_('Item'))
    order = models.ForeignKey(Order, on_delete=models.CASCADE, verbose_name=_('Order'))
    count = models.PositiveSmallIntegerField(_('Count'))

    class Meta:
        verbose_name = _('Order Item')
        verbose_name_plural = _('Order Items')
        constraints = [
            models.UniqueConstraint(fields=['item', 'order'], name='unique item in order')
        ]
