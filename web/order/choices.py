from django.db.models import IntegerChoices, TextChoices
from django.utils.translation import gettext_lazy as _


class OrderType(TextChoices):
    HUMANITARIAN = ('humanitarian', _('Humanitarian aid'))
    MILITARY = ('military', _('Military assistance'))
    TRANSPORT = ('transportation', _('Transportation'))


class OrderStatus(IntegerChoices):
    IN_BASKET = 1, _('Put in the basket')
    CREATED = 2, _('Accepted')
    IN_PROGRESS = 3, _('In processing')
    ON_THE_WAY = 4, _('On the way')
    IN_WORK = 5, _('In work')
    INVOICED = 6, _('Invoiced')
    SHIPPED = 7, _('Sent to the client')
    WAITING_FOR_PAYMENT = 8, _('Waiting for payment')
    REJECT = 9, _('Supplier cancelled')
    CUSTOMER_REJECTION = 10, _('Customer rejection')
    RETURN_FROM_CUSTOMER = 11, _('Return from customer')
    PRICE_INCREASE = 12, _('Price increase')
    ORDERED_FROM_SUPPLIER = 13, _('Ordered from supplier')
    IN_RESERVE = 14, _('In reserve')
    REMOVED_FROM_RESERVE = 15, _('Removed from reserve')
    PURCHASED = 16, _('Purchased')
    TRANSFER_TO_OTHER_ORDER = 17, _('Transfer to other order')
    STORAGE_AT_THE_SUPPLIER = 18, _('Shortage at the supplier')
    OVERDUE_DEPT = 19, _('Overdue debt')
    ORDERING_THROUGH_THE_MOVE = 20, _('Ordering through the move')
    PRE_ORDERED = 21, _('Pre-ordered')
